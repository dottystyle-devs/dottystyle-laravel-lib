@component('dottystyle::form.password', compact(
    'id', 
    'password_name', 
    'placeholder', 
    'value'
) + [
    'maskedClass' => 'fa fa-eye',
    'unmaskedClass' => 'fa fa-eye-slash'
])
    <i class="toggle toggle-mask"></i>
@endcomponent