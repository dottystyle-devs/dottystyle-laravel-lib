<div id="{{ $id }}-wrapper" class="password-wrapper">
    <input 
        id="{{ $id }}"
        class="form-control password" 
        name="{{ $password_name ?? 'password' }}" 
        type="password" 
        placeholder="{{ $placeholder ?? 'Password' }}" 
        value="{{ $value ?? '' }}" />
    {{ $slot }}
</div>
<script>
  {{ $JS_APP }}.password('#{{ $id }}-wrapper', @json([
    'maskedClass' => $maskedClass,
    'unmaskedClass' => $unmaskedClass
  ]));
</script>