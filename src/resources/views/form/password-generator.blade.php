<div class="password-generator {{ $class ?? '' }}" id="{{ $id }}-generator">
    @if (!isset($password))
        @include('dottystyle::form.fa-password', compact('id', 'password_name', 'placeholder', 'value'))
    @else 
        {{ $password }}
    @endif

    @if (!isset($button))
        <button type="button" class="generate {{ $buttonClass ?? '' }}">{{ $buttonLabel ?? 'Generate Password' }}</button>
    @else
        {{ $button }}
    @endif
</div>

<script>
    {{ $JS_APP }}.passwordGenerator('#{{ $id }}-generator');
</script>