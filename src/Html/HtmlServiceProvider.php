<?php

namespace Dottystyle\Laravel\Html;

use Collective\Html\HtmlServiceProvider as CollectiveHtmlServiceProvider;

class HtmlServiceProvider extends CollectiveHtmlServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {      
        $this->publishes([
            __DIR__.'/config.php' => config_path('html.php')
        ]);

        $this->mergeConfigFrom(__DIR__.'/config.php', 'html');
    }

    /**
     * Register the form builder instance.
     *
     * @return void
     */
    protected function registerFormBuilder()
    {
        $this->app->singleton('form', function ($app) {
            $class = $app['config']->get('html.form');
            $form = new $class($app['html'], $app['url'], $app['view'], $app['session.store']->token(), $app['request']);

            return $form->setSessionStore($app['session.store']);
        });
    }
}
