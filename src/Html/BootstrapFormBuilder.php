<?php

namespace Dottystyle\Laravel\Html;

use Collective\Html\FormBuilder as CollectiveFormBuilder;

class BootstrapFormBuilder extends CollectiveFormBuilder
{
    /**
     * @var string
     */
    protected $formElementIdPrefix = '';

    /**
     * @var bool
     */
    protected $roundedButtons = true;

    /**
     * @inheritdoc
     */
    public function input($type, $name, $value = null, $options = [])
    {
        return parent::input($type, $name, $value, $this->mergeFormElementClass($options, $type));
    }

    /**
     * @inheritdoc
     */
    public function textarea($name, $value = null, $options = [])
    {
        return parent::textarea($name, $value, $this->mergeFormElementClass($options, 'textarea'));
    }

    /**
     * @inheritdoc
     */
    public function select(
        $name,
        $list = [],
        $selected = null,
        array $selectAttributes = [],
        array $optionsAttributes = [],
        array $optgroupsAttributes = []
    ) {
        return parent::select(
            $name, 
            $list, 
            $selected, 
            $this->mergeFormElementClass($selectAttributes, 'select'), 
            $optgroupsAttributes, 
            $optgroupsAttributes
        );
    }

    /**
     * @inheritdoc
     */
    public function label($name, $value = null, $options = [], $escapeHtml = true)
    {
        if ($this->formElementIdPrefix) {
            $name = $this->getIdWithPrefixAttribute($name);
        }

        if (isset($options['required'])) {
            $value .= '*';
            unset($options['required']);
        }

        return parent::label($name, $value, $this->mergeClassToAttributeOptions($options, 'control-label'), $escapeHtml);
    }

    /**
     * Merge class to form element attributes.
     * 
     * @param array $options
     * @param string $type
     * @return array
     */
    protected function mergeFormElementClass(array $options, $type)
    {
        if (in_array($type, ['radio', 'checkbox'])) {
            $className = 'form-check-input';
        } else {
            $className = 'form-control';
        }

        return $this->mergeClassToAttributeOptions($options, $className);
    }

    /**
     * Merge class or classes to attribute options.
     * 
     * @param array $options
     * @param array|string $class
     * @return array
     */
    protected function mergeClassToAttributeOptions(array $options, $class)
    {
        if (isset($options['class'])) {
            $class = array_merge((array) $options['class'], (array) $class);
        }

        $options['class'] = $class;

        return $options;
    }

    /**
     * @inheritdoc
     */
    public function open(array $options = array())
    {
        if (isset($options['id']) && $options['id']) {
            $this->formElementIdPrefix = $options['id'];
        }

        return parent::open($options);
    }

    /**
     * @inheritdoc
     */
    public function close()
    {
        // Clear element id prefix upon closing
        $this->formElementIdPrefix = '';

        return parent::close();
    }

    /**
     * Get the ID attribute for a field name.
     *
     * @param  string $name
     * @param  array  $attributes
     *
     * @return string
     */
    public function getIdAttribute($name, $attributes)
    {
        $value = parent::getIdAttribute($name, $attributes);

        if (empty($value) && in_array($prefixed = $this->getIdWithPrefixAttribute($name), $this->labels)) {
            return $prefixed;
        }

        return $value;
    }

    /**
     * Get an id attribute value prefixed with the form id.
     * 
     * @param string $value
     * @return string
     */
    protected function getIdWithPrefixAttribute($value)
    {
        return $this->formElementIdPrefix.'-'.$value;
    }

    /**
     * @inheritdoc
     */
    public function submit($value = null, $options = [])
    {
        return $this->primaryButton($value ?? __('Submit'), array_merge($options, ['type' => 'submit']));
    }

    /**
     * @inheritdoc
     */
    public function button($value = null, $options = [])
    {
        $classNames = array_merge(['btn'], $this->roundedButtons ? ['btn-round'] : []);

        return parent::button($value, $this->mergeClassToAttributeOptions($options, $classNames));
    }

    /**
     * Create a primary button.
     * 
     * @param  string $value
     * @param  array  $options
     * @return \Illuminate\Support\HtmlString
     */
    public function primaryButton($value = null, array $options = [])
    {
        return $this->button($value, $this->mergeClassToAttributeOptions($options, 'btn-primary'));
    }

    /**
     * Create a secondary button.
     * 
     * @param  string $value
     * @param  array  $options
     * @return \Illuminate\Support\HtmlString
     */
    public function secondaryButton($value = null, array $options = [])
    {
        return $this->button($value, $this->mergeClassToAttributeOptions($options, 'btn-secondary'));
    }

    /**
     * Sets whether the buttons are rounded or not.
     * 
     * @param bool $value
     * @return self
     */
    public function roundButtons($value)
    {
        $this->roundedButtons = $value;

        return $this;
    }
}