<?php

namespace Dottystyle\Laravel\Foundation;

use Dottystyle\Laravel\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * 
     * @return void
     */
    public function register()
    {
    }
}