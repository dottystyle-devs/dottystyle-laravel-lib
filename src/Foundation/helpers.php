<?php

if (! function_exists('json_success')) {
    /**
     * Return a successful json response from the application.
     * 
     * @param array $data (optional)
     * @param int $status (optional)
     * @param array $headers (optional)
     * @return unknown
     */
    function json_success(array $data = [], $status = 200, $headers = []) 
    {
        return response()->json(['success' => true] + $data)
            ->setStatusCode($status)
            ->withHeaders($headers);
    }
}

if (! function_exists('json_failure')) {
    /**
     * Return a failed json response from the application.
     * 
     * @param string|null $message (optional)
     * @param int $status (optional)
     * @param array $headers (optional)
     * @return unknown
     */
    function json_failure(string $message = null, $status = 400, $headers = []) 
    {
        $payload = ['success' => false];
        $message and ($payload['message'] = $message);

        return response()->json($payload)
            ->setStatusCode($status)
            ->withHeaders($headers);
    }
}