<?php

return [
    'client_script_data' => [
        /**
         * @var string
         */
        'view' => '',

        /**
         * @var string 
         */
        'variable_name' => 'appData'
    ],

    /**
     * @var string
     */
    'client_script_app_variable' => 'App'
];