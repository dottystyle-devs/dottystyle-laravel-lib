<?php

namespace Dottystyle\Laravel\View;

use Dottystyle\Laravel\Contracts\ClientScriptDataProvider;

class BasicClientScriptDataProvider implements ClientScriptDataProvider
{
    /**
     * @var array
     */
    protected $extras = [];

    /**
     * Include the application url, name, and debug flag.
     * 
     * @return array
     */
    public function getData() 
    {
        // Add default configs
        return [
            'url' => config('app.url'),
            'name' => config('app.name'),
            'debug' => config('app.debug')
        ] + $this->extras;
    }

    /**
     * 
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function addData(string $key, $value)
    {
        $this->extras[$key] = $value;
    }
}