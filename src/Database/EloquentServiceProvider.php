<?php

namespace Dottystyle\Laravel\Database;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Closure;

class EloquentServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     * 
     * @return void
     */
    public function boot()
    {
        Builder::macro('joinRelation', $this->createMacro(new Eloquent\JoinFromRelationship('join')));
        Builder::macro('leftJoinRelation', $this->createMacro(new Eloquent\JoinFromRelationship('leftJoin')));
        Builder::macro('rightJoinRelation', $this->createMacro(new Eloquent\JoinFromRelationship('rightJoin')));

        /**
         * Group the current builder's where clause.
         */
        QueryBuilder::macro('groupWheres', function (Closure $callback = null) {
            $wheres = $this->wheres;
            $bindings = $this->bindings['where'];
            $this->wheres = [];
            $this->bindings['where'] = [];

            $this->where(function ($subQuery) use ($wheres, $bindings, $callback) {
                $subQuery->mergeWheres($wheres, $bindings);

                if ($callback) {
                    $callback($subQuery);
                }
            });

            return $this;
        });

        /**
         * Add macro to convert a collection of models to list good for dropdowns.
         */
        Collection::macro('options', function ($label = 'name', $value = null) {
            $value = $value ?: $this->first()->getKeyName();

            return $this->map(function ($model) use ($label, $value) {
                return [
                    'label' => $model->{$label}, 
                    'value' => $model->{$value}
                ];
            })->values();
        });
    }

    /**
     * Create a closure for the macro.
     * 
     * @param \Dottystyle\Laravel\Database\Eloquent\JoinFromRelationship
     * @return \Closure
     */
    protected function createMacro($macro)
    {
        return function ($relation, array $columns = ['*'], Closure $handleJoin = null) use ($macro) {
            $args = func_get_args();
            array_splice($args, 1, 0, [$this]);

            return call_user_func_array($macro, $args);
        };
    }
    
    /**
     * Register application services 
     * 
     * @return void
     */
    public function register()
    {
    }
}