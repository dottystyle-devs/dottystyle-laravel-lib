<?php

namespace Dottystyle\Laravel\Database;

use Illuminate\Support\Facades\DB;

trait UsesDbTransactionTrait
{
    /**
     * Call the given callback inside a database transaction and return its result.
     * 
     * @param \Closure $callback
     * @param bool $rethrow (optional)
     * @param mixed $failed (optional)
     * @return mixed
     */
    protected function trans($callback, $rethrow = true, $failed = null)
    {
        try {
            DB::beginTransaction();

            $result = call_user_func($callback, $commit = $this->createDBTransCommitCallback());
            $commit();

            return $result;
        } catch (Exception $e) {
            DB::rollback();

            if ($rethrow) {
                throw $e;
            }

            report($e);

            if ($failed instanceof Closure) {
                return $failed($e);
            } else {
                return $failed;
            }
        }
    }

    /**
     * Create a closure to fire a database transaction commit if not yet fired.
     * 
     * @return \Closure
     */
    protected function createDBTransCommitCallback()
    {
        $committed = false;

        return function () use (&$committed) {
            if (! $committed) {
                DB::commit();
                $committed = true;
            }
        };
    }
}