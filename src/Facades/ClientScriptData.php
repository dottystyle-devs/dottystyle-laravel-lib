<?php

namespace Dottystyle\Laravel\Facades;

use Illuminate\Support\Facades\Facade;
use Dottystyle\Laravel\Contracts\ClientScriptDataProvider;

class ClientScriptData extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ClientScriptDataProvider::class;
    }
}