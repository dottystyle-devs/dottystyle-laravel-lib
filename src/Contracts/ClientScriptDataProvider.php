<?php

namespace Dottystyle\Laravel\Contracts;

interface ClientScriptDataProvider
{
    /**
     * Get the data to be used on client-side script.
     * 
     * @return array
     */
    public function getData();
}