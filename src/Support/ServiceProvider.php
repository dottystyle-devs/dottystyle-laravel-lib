<?php

namespace Dottystyle\Laravel\Support;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

abstract class ServiceProvider extends LaravelServiceProvider
{
    /**
     * @var string
     */
    protected $packageNamespace = 'dottystyle';

    /**
     * 
     * 
     * @param string $append (optional)
     * @param string $glue (optional)
     * @return string
     */
    protected function ns($append = '', $glue = '/')
    {
        $path = $this->packageNamespace;
        $append and ($path .= $glue.$append);

        return $path;
    }

    /**
     * 
     * @param string $key
     * @return string
     */
    protected function getConfigKey(string $key)
    {
        return $this->ns($key, '.');
    }
}