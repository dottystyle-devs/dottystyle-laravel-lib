<?php

namespace Dottystyle\Laravel\Http;

trait ConvertsToResource
{
    /**
     * Convert the model into a resource object using the assigned resource class.
     * 
     * @param \Illuminate\Http\Request $request
     * @param \Callable $callback
     * @return \Dottystyle\Laravel\Http\ResourceBuilder
     */
    public function toResource($request = null)
    {
        return new ResourceBuilder($this, $this->getResourceClass());
    }

    /**
     * Convert the model into a resource object using the assigned resource class and given parameters.
     * 
     * @param array $parameters
     * @param \Illuminate\Http\Request $request (optional)
     * @return \App\Models\ResourceBuilder
     */
    public function toResourceWithParams(array $parameters, $request = null)
    {
        return new ResourceBuilder($this, $this->getResourceClass(), $parameters);
    }

    /**
     * Get the class that will be used to convert the model into resource.
     * 
     * @return string
     */
    public function getResourceClass()
    {
        return $this->resourceClass;
    }

    /**
     * Helper method to get an attribute or multiple attributes from the resource.
     * 
     * @param array|string $key
     * @return mixed
     */
    public function getResourceAttribute($key)
    {
        $data = $this->toResource()->toArray();

        return is_array($key) ? Arr::only($data, $key) : $data[$key];
    }
} 