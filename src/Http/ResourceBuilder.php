<?php

namespace Dottystyle\Laravel\Http;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Traits\ForwardsCalls;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Responsable;

class ResourceBuilder implements Arrayable, Responsable
{
    use ForwardsCalls;

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var string
     */
    protected $resourceClass;

    /**
     * @var \Illuminate\Http\Resources\Json\Resource
     */
    protected $resource;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * @var \Illuminate\Http\Request $request
     */
    protected $request;

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $resourceClass
     * @param array (optional)
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Model $model, $resourceClass, $parameters = [], $request = null)
    {
        $this->model = $model;
        $this->resourceClass = $resourceClass;
        $this->parameters = $parameters;
        $this->request = $request;
    }

    /**
     * Get the resource object.
     * Creates the resource object once.
     * 
     * @return \Illuminate\Http\Resources\Json\Resource
     */
    protected function getResource()
    {
        if (empty($this->resource)) {
            // Create a new instance of the resource given the model instance and other parameters
            $this->resource = call_user_func_array(
                [$this->resourceClass, 'make'], array_merge([$this->model], $this->parameters)
            );
        }

        return $this->resource;
    }

    /**
     * Set the extra parameters required to create the resource.
     * 
     * @param array $parameters
     * @return static 
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Forward all other method calls to the resource object.
     * 
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call($method, $parameters) 
    {
        return $this->forwardCallTo($this->getResource(), $method, $parameters);
    }

    /**
     * Convert the underlying resource object to array.
     * 
     * @param \Illuminate\Http\Request $request (optional)
     * @return array
     */
    public function toArray()
    {
        return $this->getResource()->resolve($this->request);
    }

    /**
     * Create an HTTP response that represents the object.
     * The resource builder can be returned as the route response directly without
     * the need to access the resource object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        return $this->getResource()->toResponse($request);
    }
}